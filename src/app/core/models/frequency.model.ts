export interface FrequencyModel {
  id: number;
  name: string;
  noOfDays: number;
}
