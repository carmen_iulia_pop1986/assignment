import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";

import { DosageModel } from "../models/dosage.model";
import { FrequencyModel } from "../models/frequency.model";


@Injectable()
export class MetadataService {

  private static readonly frequencies: FrequencyModel[] = [
    { id: 1, name: 'Once a week', noOfDays: 10 },
    { id: 2, name: 'Twice a week', noOfDays: 90 },
    { id: 3, name: 'Three times a week', noOfDays: null }
  ];

  private static readonly dosages: { [index: number]: DosageModel[] } = {
    1: [{ quantity: 5 }],
    2: [{ quantity: 2 }, { quantity: 3 }],
    3: [{ quantity: 2 }, { quantity: 2 }, { quantity: 1 }]
  };

}
