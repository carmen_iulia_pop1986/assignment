import { Injectable } from "@angular/core";
import { Observable, of } from "rxjs";
import { tap } from "rxjs/operators";

import { MedicationModel } from "../models/medication.model";


@Injectable()
export class MedicationService {

  /**
   * Save a medication
   *
   * @param medication
   *     The medication to save.
   * @return
   *     An observable to save the medication.
   */
  saveMedication(medication: MedicationModel): Observable<void> {
    return of(undefined).pipe(
      tap(() => console.log('Saved medication', medication))
    );
  }

}
